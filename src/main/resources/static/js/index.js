function btnClick(path) {

    setLabel("Загрузка...");
    $.ajax({
        type: "POST",
        url: path,
        data: {
            token: "50582c48-a438-405e-87be-f61dff474108"
        }
    })
    .done(function(result) {
        setLabel("Удачно");
        console.log(result);
    })
    .fail(function(result) {
        setLabel("Не удачно");
        console.log(result);
    });
}

function setLabel(text) {
    $('#result').text(text);
}