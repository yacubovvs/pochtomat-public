package ru.cubos.pochtomat.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cubos.pochtomat.repositories.FeedbackTemplateRepository;
import ru.cubos.pochtomat.services.FeedbackService;
import ru.cubos.pochtomat.services.FeedbackTemplateService;

import java.util.UUID;

@Tag(
        name = "Контроллер обратной связи",
        description = "Контроллер обратной связи"
)

@RestController
@RequestMapping("/feedback")
@AllArgsConstructor
public class FeedbackController {

    private final FeedbackService feedbackService;

    @GetMapping("/{terminalUUID}")
    String getFeedbackBody(@PathVariable("terminalUUID") UUID terminalUUID){
        return feedbackService.getFeedbackForm(terminalUUID);
    }

}
