package ru.cubos.pochtomat.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cubos.pochtomat.repositories.FeedbackTemplateRepository;
import ru.cubos.pochtomat.services.FeedbackTemplateService;

import java.util.NoSuchElementException;

@Tag(
        name = "Контроллер работы с шаблонами форм",
        description = "Контроллер работы с шаблонами форм"
)

@RestController
@RequestMapping("/templates")
@AllArgsConstructor
public class FeedbackTemplateController {
    private final FeedbackTemplateRepository feedbackTemplateRepository;
    private final FeedbackTemplateService feedbackTemplateService;

    @GetMapping("/preview/{templateId}")
    String showTemplate(@PathVariable("templateId") Long templateId){
        return feedbackTemplateService.getTemplateBody(templateId);
    }

}
