package ru.cubos.pochtomat.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@Tag(
        name = "Тестовый контролер",
        description = "Тестовый контроллер для проверки запросов"
)
@RestController
public class TestController {

    @PostMapping("/success")
    HashMap<String, ?> success() {
        HashMap<String, Object> response = new HashMap<>();
        response.put("Parameter", "some value");
        return response;
    }

    @PostMapping("/fail")
    void fail() {
        throw new IllegalArgumentException("Введены неверные данные");
    }
}
