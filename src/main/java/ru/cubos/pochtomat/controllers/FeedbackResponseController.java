package ru.cubos.pochtomat.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.cubos.pochtomat.dto.request.PostFeedbackRequestDto;
import ru.cubos.pochtomat.dto.response.FeedbackResponseRequestDto;
import ru.cubos.pochtomat.services.FeedbackResponseService;

@Tag(
        name = "Контроллер работы ответов от форм обратной связи",
        description = ""
)

@RestController
@RequestMapping("/feedback")
@AllArgsConstructor
public class FeedbackResponseController {
    private final FeedbackResponseService feedbackResponseService;

//    @PostMapping("")
//    ResponseEntity<?> addFeedback(@RequestBody FeedbackResponseRequestDto feedbackResponseRequestDto){
//        return ResponseEntity.ok(feedbackResponseService.addFeedbackResponse(feedbackResponseRequestDto));
//    }

    /**
     * Получение форм опросов
     * @param requestDto данные запроса
     * @return ответ успешно / неудачно
     */
    @PostMapping("")
    ResponseEntity<?> addFeedback(@RequestBody PostFeedbackRequestDto requestDto){
        return ResponseEntity.ok(feedbackResponseService.addFeedbackResponse(requestDto));
    }
}
