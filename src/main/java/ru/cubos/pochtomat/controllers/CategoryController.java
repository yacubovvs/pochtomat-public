package ru.cubos.pochtomat.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cubos.pochtomat.dto.request.GetCategoryByCommentRequestDto;
import ru.cubos.pochtomat.services.AICategoryService;

@Tag(
        name = "Контроллер категорий комментария",
        description = "Метод определения категорий комментариев"
)

@RestController
@RequestMapping("/category")
@AllArgsConstructor
public class CategoryController {
    private final AICategoryService aiCategoryService;

    @PostMapping("/categoryByComment")
    ResponseEntity<?> getCategoryByComment(@RequestBody GetCategoryByCommentRequestDto requestDto){
        return ResponseEntity.ok(aiCategoryService.getCommentCategories(requestDto));
    }
}
