package ru.cubos.pochtomat.utils;

import ru.cubos.pochtomat.dto.response.BaseResponse;

public class ErrorUtils {
    static public BaseResponse noSuchElement (String errorMessage) {
        return new BaseResponse()
                .setErrorCode("404")
                .setErrorMessage(errorMessage)
                .setSuccess(false);
    }
}
