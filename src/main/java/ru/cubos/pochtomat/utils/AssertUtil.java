package ru.cubos.pochtomat.utils;

import lombok.SneakyThrows;
import ru.cubos.pochtomat.dto.response.BaseResponse;

public class AssertUtil {
    @SneakyThrows
    public static <T extends BaseResponse> T assertNotNull(final T response,
                                                           final Object object,
                                                           final String errorMessage,
                                                           String errorCode ){
        if( response.getSuccess() == false) {
            return response;
        }

        if (object == null) {
            T newobject=(T)response.getClass().newInstance();
            newobject.setSuccess(false);
            newobject.setErrorCode(errorCode);
            newobject.setErrorMessage(errorMessage);
            return newobject;
        } else {
            return response;
        }
    }

    public static <T extends BaseResponse> T assertNotNull(final T response,
                                             final Object object,
                                             final String errorMessage) {
        return assertNotNull(response, object, errorMessage, "401");
    }
}
