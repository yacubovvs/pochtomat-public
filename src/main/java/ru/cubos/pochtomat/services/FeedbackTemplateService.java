package ru.cubos.pochtomat.services;

public interface FeedbackTemplateService {

    String getTemplateBody(Long templateId);
}
