package ru.cubos.pochtomat.services;

import java.util.UUID;

public interface FeedbackService {

    String getFeedbackForm(UUID terminalUUID);
}
