package ru.cubos.pochtomat.services;

import ru.cubos.pochtomat.dto.request.GetCategoryByCommentRequestDto;
import ru.cubos.pochtomat.dto.response.GetCategoryByCommentResponseDto;

public interface AICategoryService {
    GetCategoryByCommentResponseDto getCommentCategories(GetCategoryByCommentRequestDto requestDto);
}
