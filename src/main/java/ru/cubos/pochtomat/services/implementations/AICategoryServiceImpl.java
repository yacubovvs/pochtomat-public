package ru.cubos.pochtomat.services.implementations;

import org.springframework.stereotype.Service;
import ru.cubos.dataset.analysis.Analise;
import ru.cubos.dataset.entity.DataSet;
import ru.cubos.dataset.utils.DataSetController;
import ru.cubos.pochtomat.dto.request.GetCategoryByCommentRequestDto;
import ru.cubos.pochtomat.dto.response.GetCategoryByCommentResponseDto;
import ru.cubos.pochtomat.services.AICategoryService;
import ru.cubos.pochtomat.utils.AssertUtil;

import java.util.List;

@Service
public class AICategoryServiceImpl implements AICategoryService {

    @Override
    public GetCategoryByCommentResponseDto getCommentCategories(GetCategoryByCommentRequestDto requestDto) {
        GetCategoryByCommentResponseDto responseDto = new GetCategoryByCommentResponseDto();
        responseDto = AssertUtil.assertNotNull(responseDto,
                requestDto.getComment(),
                "Комментарий не может быть пустым!");
        if (responseDto.getSuccess() == false) {
            return responseDto;
        }

        List<DataSet> dataSet = DataSetController.getDataSet("/opt/commentsDataset.csv");

        Analise analise = new Analise();
        analise.train(dataSet);

        responseDto = new GetCategoryByCommentResponseDto()
                .setCategories(analise.feed(requestDto.getComment()));

        return responseDto;
    }
}
