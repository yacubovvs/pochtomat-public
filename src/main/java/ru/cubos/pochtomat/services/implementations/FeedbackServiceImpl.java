package ru.cubos.pochtomat.services.implementations;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.cubos.pochtomat.dto.entities.FeedbackTemplate;
import ru.cubos.pochtomat.repositories.FeedbackTemplateRepository;
import ru.cubos.pochtomat.services.FeedbackService;

import java.util.UUID;

@Service
@AllArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackTemplateRepository feedbackTemplateRepository;

    @Override
    public String getFeedbackForm(UUID terminalUUID) {

        FeedbackTemplate feedbackTemplate =
                feedbackTemplateRepository.getFeedbackTemplateByTerminalUUID(terminalUUID)
                        .orElseThrow(() -> new IllegalArgumentException(
                                String.format("Терминал %s не найден", terminalUUID)));

        String template = feedbackTemplate.getTemplate();

        template = template.replaceAll(":terminalUUID", terminalUUID.toString());
        template = template.replaceAll(":feedbackTemplateId", feedbackTemplate.getId().toString());

        return template;
    }
}
