package ru.cubos.pochtomat.services.implementations;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.cubos.pochtomat.dto.entities.*;
import ru.cubos.pochtomat.dto.request.GetCategoryByCommentRequestDto;
import ru.cubos.pochtomat.dto.request.PostFeedbackRequestDto;
import ru.cubos.pochtomat.dto.response.BaseResponse;
import ru.cubos.pochtomat.repositories.*;
import ru.cubos.pochtomat.services.FeedbackResponseService;
import ru.cubos.pochtomat.utils.AssertUtil;
import ru.cubos.pochtomat.utils.ErrorUtils;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class FeedbackResponseServiceImpl implements FeedbackResponseService {

    private final TerminalRepository terminalRepository;
    private final FeedbackResponseRepository feedbackResponseRepository;
    private final ApiTokenRepository apiTokenRepository;
    private final FeedbackTemplateRepository feedbackTemplateRepository;
    private final FeedbackSourceRepository feedbackSourceRepository;
    private final CategoryTypeRepository categoryTypeRepository;
    private final AICategoryServiceImpl aiCategoryService;

    @Override
    @Transactional
    public BaseResponse addFeedbackResponse(PostFeedbackRequestDto requestDto) {
        BaseResponse response = new BaseResponse();

        response = AssertUtil.assertNotNull(response,
                requestDto.getTerminalUUID(),
                "TerminalUUID не должен быть пустым");
        response = AssertUtil.assertNotNull(response,
                requestDto.getValue(),
                "Value не должен быть пустым");
        response = AssertUtil.assertNotNull(response,
                requestDto.getFeedbackSourceCode(),
                "FeedbackSource не должен быть пустым");

        if (response.getSuccess() == false) {
            return response;
        }

        Terminal terminal = terminalRepository
            .findFirstByUuid(requestDto.getTerminalUUID());

        if (terminal == null) {
            return ErrorUtils.noSuchElement(String.format("Терминал не найден по UUID: %s",
                    requestDto.getTerminalUUID()));
        }

        ApiToken apiToken = apiTokenRepository.findActiveToken(requestDto.getApiToken());

        if (apiToken == null) {
            return ErrorUtils.noSuchElement(String.format("Токен не найден по UUID: %s",
                    requestDto.getApiToken()));
        }

        if (requestDto.getFeedbackTemplateId() != null) {
            FeedbackTemplate feedbackTemplate = feedbackTemplateRepository
                    .findFirstById(requestDto.getFeedbackTemplateId()).get();

            if (feedbackTemplate == null) {
                return ErrorUtils.noSuchElement(String.format(
                        "Шаблон формы ответа не найден по id: %s",
                        requestDto.getFeedbackTemplateId()));
            }
        }

        FeedbackSource feedbackSource = feedbackSourceRepository
                .findFirstByCode(requestDto.getFeedbackSourceCode());

        if (feedbackSource == null) {
            return ErrorUtils.noSuchElement(String.format(
                    "Источник обратной связи не найден по Code: %s",
                    requestDto.getFeedbackSourceCode()));
        }

        LocalDateTime timestamp = requestDto.getTimestamp() == null ?
                LocalDateTime.now() :
                requestDto.getTimestamp();

        FeedbackResponse feedbackResponse = new FeedbackResponse()
                .setTerminalId(terminal.getId())
                .setFeedbackTemplateId(requestDto.getFeedbackTemplateId())
                .setFeedbackSourceId(feedbackSource.getId())
                .setApiTokenId(apiToken.getId())
                .setValue(requestDto.getValue())
                .setValueStr(requestDto.getValueStr())
                .setPhone(requestDto.getPhone())
                .setClient(requestDto.getClient())
                .setComment(requestDto.getComment())
                .setTimestamp(timestamp)
                .setOrderNum(requestDto.getOrderNum());

        feedbackResponseRepository.save(feedbackResponse);
        requestDto.setCategories(requestDto.getCategories()
                .stream().filter(e -> e != null).toList());

        if (requestDto.getCategories().size() > 0) {
            requestDto.getCategories().forEach(categoryDto ->
                    feedbackResponseRepository.addCategoryToFeedback(
                            feedbackResponse.getId(),
                            categoryTypeRepository
                                    .findFirstByCategory(categoryDto.getNum().toString()).getId()));
        } else {
            if (requestDto.getDetectCategories() == true) {
                aiCategoryService.getCommentCategories(
                                new GetCategoryByCommentRequestDto()
                                        .setComment(requestDto.getComment()))
                        .getCategories().forEach(categoryDto ->
                                feedbackResponseRepository.addCategoryToFeedback(
                                        feedbackResponse.getId(),
                                        categoryTypeRepository
                                                .findFirstByCategory("" + categoryDto.getNum())
                                                .getId()));
            }
        }

        return new BaseResponse().setSuccess(true)
                .setMessage(String.format("Ответ формы обратной записи записан с Id: %s",
                        feedbackResponse.getId()));
    }
}
