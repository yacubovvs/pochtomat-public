package ru.cubos.pochtomat.services.implementations;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.cubos.pochtomat.repositories.FeedbackTemplateRepository;
import ru.cubos.pochtomat.services.FeedbackTemplateService;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class FeedbackTemplateServiceImpl implements FeedbackTemplateService {

    private final FeedbackTemplateRepository feedbackTemplateRepository;

    @Override
    public String getTemplateBody(Long templateId) {
        return feedbackTemplateRepository.findFirstById(templateId)
                .orElseThrow( () -> new NoSuchElementException(
                        String.format("Шаблон с id %s не найден", templateId)))
                .getTemplate();
    }

}
