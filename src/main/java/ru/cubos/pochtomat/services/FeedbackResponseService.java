package ru.cubos.pochtomat.services;

import ru.cubos.pochtomat.dto.request.PostFeedbackRequestDto;
import ru.cubos.pochtomat.dto.response.BaseResponse;
import ru.cubos.pochtomat.dto.response.FeedbackResponseRequestDto;

public interface FeedbackResponseService {
    BaseResponse addFeedbackResponse(PostFeedbackRequestDto requestDto);
}
