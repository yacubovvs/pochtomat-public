package ru.cubos.pochtomat.dto.response;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.cubos.dataset.entity.Category;
import ru.cubos.pochtomat.dto.response.BaseResponse;

import java.util.Set;

@Data
@Accessors(chain = true)
public class GetCategoryByCommentResponseDto extends BaseResponse {
    private Set<Category> categories;
}
