package ru.cubos.pochtomat.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class FeedbackResponseRequestDto {

    @JsonProperty("terminalUUID")
    private UUID terminalUUID;

    @JsonProperty("templateId")
    private Long templateId;

    @JsonProperty("value")
    private Integer value;

    @JsonProperty("valueStr")
    private String valueStr;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("client")
    private String client;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("timestamp")
    private LocalDateTime timestamp;

}
