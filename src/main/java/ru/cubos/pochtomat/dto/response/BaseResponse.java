package ru.cubos.pochtomat.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BaseResponse {
    @JsonProperty("success")
    Boolean success = true;

    @JsonProperty("errorCode")
    String errorCode = "";

    @JsonProperty("errorMessage")
    String errorMessage = "";

    @JsonProperty("message")
    String message = "";
}
