package ru.cubos.pochtomat.dto.entities;

public enum Category {

    GOODS_QUALITY("Качество товара", 1L),
    GOODS_COMPOSITION("Состав товара", 2L),
    DELIVERY_TIME("Срок доставки", 3L),
    PACKAGING("Качество упаковки", 4L),
    POSTOMAT_WORK("Работа постамата", 5L),
    NO_CATEGORY("Без категории", 0L);

    private String id;
    private Long num;

    Category(String id, Long num) {
        this.id = id;
        this.num = num;
    }

    public String getId() {
        return id;
    }

    public static Category fromId(String id) {
        for (Category at : Category.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
}