package ru.cubos.pochtomat.dto.entities;

public enum IncidentsStage {

    CREATE("Created"),
    IN_PROGRESS("In progress"),
    PENDING("Pending"),
    CLOSED("Closed");

    private String id;

    IncidentsStage(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public static IncidentsStage fromId(String id) {
        for (IncidentsStage at : IncidentsStage.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}