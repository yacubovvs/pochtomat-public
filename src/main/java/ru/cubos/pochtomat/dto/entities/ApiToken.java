package ru.cubos.pochtomat.dto.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Accessors(chain = true)
@Table(name = "POCH__API_TOKEN")
@Entity
public class ApiToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "UUID")
    private UUID uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "feedback_source_id")
    private Long feedbackSourceId;

    @Column(name = "CREATED_DATE")
    private LocalDateTime createdDate;

    @Column(name = "EXPIRED_DATE")
    private LocalDateTime expiredDate;

    @Column(name = "enabled")
    private Boolean enabled;
}