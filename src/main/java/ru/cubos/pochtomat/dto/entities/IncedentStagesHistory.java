package ru.cubos.pochtomat.dto.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "POCH__INCEDENT_STAGES_HISTORY")
public class IncedentStagesHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "incident_id")
    private Long incidentId;

    @Column(name = "stage")
    private String stage;

    @Column(name = "comment")
    private String comment;

    @Column(name = "responsible_employee_id")
    private UUID responsibleEmployeeId;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

}