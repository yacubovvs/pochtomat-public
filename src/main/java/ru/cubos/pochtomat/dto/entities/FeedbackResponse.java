package ru.cubos.pochtomat.dto.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "POCH__FEEDBACK_RESPONSE")
public class FeedbackResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "terminal_id")
    private Long terminalId;

    @Column(name = "feedback_template_id")
    private Long feedbackTemplateId;

    @Column(name = "feedbackSourceId")
    private Long feedbackSourceId;

    @Column(name = "apiTokenId")
    private Long apiTokenId;

    @Column(name = "value")
    private Integer value;

    @Column(name = "value_str")
    private String valueStr;

    @Column(name = "phone")
    private String phone;

    @Column(name = "client")
    private String client;

    @Column(name = "comment")
    private String comment;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Column(name = "order_num")
    private String orderNum;
}
