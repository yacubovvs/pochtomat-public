package ru.cubos.pochtomat.dto.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Table(name = "poch__Terminal")
public class Terminal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "UUID", nullable = false)
    private UUID uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @JoinColumn(name = "default_template_id")
    private Long defaultTemplateId;

    @Column(name = "enable")
    private Boolean enable;

}
