package ru.cubos.pochtomat.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.cubos.pochtomat.dto.entities.Category;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class PostFeedbackRequestDto {
    @JsonProperty("terminalUUID")
    private UUID terminalUUID;

    @JsonProperty("feedbackTemplateId")
    private Long feedbackTemplateId;

    @JsonProperty("feedbackSourceCode")
    private String feedbackSourceCode;

    @JsonProperty("apiToken")
    private UUID apiToken;

    @JsonProperty("value")
    private Integer value;

    @JsonProperty("valueStr")
    private String valueStr;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("client")
    private String client;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("timestamp")
    private LocalDateTime timestamp;

    @JsonProperty("categories")
    private List<Category> categories;

    @JsonProperty("detectCategories")
    private Boolean detectCategories;

    @JsonProperty("orderNum")
    private String orderNum;
}
