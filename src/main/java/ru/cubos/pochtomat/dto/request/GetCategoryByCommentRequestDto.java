package ru.cubos.pochtomat.dto.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GetCategoryByCommentRequestDto {
    private String comment;
}
