package ru.cubos.pochtomat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.cubos.dataset.analysis.Analise;
import ru.cubos.dataset.entity.DataSet;
import ru.cubos.dataset.utils.DataSetController;

import java.util.List;

@SpringBootApplication
public class PochtomatApplication {

	public static void main(String[] args) {
		SpringApplication.run(PochtomatApplication.class, args);
	}

}
