package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.cubos.pochtomat.dto.entities.Category;
import ru.cubos.pochtomat.dto.entities.CategoryType;

public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {
    CategoryType findFirstByCategory(String category);
}
