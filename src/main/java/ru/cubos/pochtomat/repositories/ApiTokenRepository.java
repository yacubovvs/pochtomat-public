package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.cubos.pochtomat.dto.entities.ApiToken;

import java.time.LocalDateTime;
import java.util.UUID;

public interface ApiTokenRepository extends JpaRepository<ApiToken, Long> {
    @Query( " SELECT " +
            "   apiToken " +
            " FROM" +
            "   ApiToken apiToken" +
            " WHERE " +
            "   apiToken.uuid = :uuid " +
            "   AND ( " +
            "       apiToken.expiredDate IS NULL " +
            "       OR apiToken.expiredDate < now() " +
            "   ) " +
            "   AND apiToken.enabled = true ")
    ApiToken findActiveToken(UUID uuid);
}
