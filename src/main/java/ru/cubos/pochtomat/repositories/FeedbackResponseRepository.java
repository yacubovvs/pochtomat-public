package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.cubos.pochtomat.dto.entities.FeedbackResponse;

public interface FeedbackResponseRepository extends JpaRepository<FeedbackResponse, Long> {
    @Modifying
    @Query(value = " INSERT INTO " +
            " public.poch__response_comment_category_link " +
            "   (feedback_response_id, category_type_id) " +
            " VALUES (:feedbackResponseId, :categoryTypeId) ", nativeQuery = true)
    void addCategoryToFeedback(Long feedbackResponseId, Long categoryTypeId);
}
