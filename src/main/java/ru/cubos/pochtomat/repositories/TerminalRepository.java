package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.cubos.pochtomat.dto.entities.Terminal;

import java.util.UUID;

public interface TerminalRepository extends JpaRepository<Terminal, Long> {
    Terminal findFirstByUuid(UUID uuid);
}
