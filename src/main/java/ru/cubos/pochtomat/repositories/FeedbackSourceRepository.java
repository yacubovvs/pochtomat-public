package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.cubos.pochtomat.dto.entities.FeedbackSource;

public interface FeedbackSourceRepository extends JpaRepository<FeedbackSource, Long> {
    FeedbackSource findFirstByCode(String sourceCode);
}
