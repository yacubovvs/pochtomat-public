package ru.cubos.pochtomat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.cubos.pochtomat.dto.entities.FeedbackTemplate;

import java.util.Optional;
import java.util.UUID;

public interface FeedbackTemplateRepository extends JpaRepository<FeedbackTemplate, Long> {

    Optional<FeedbackTemplate> findFirstById(Long aLong);

    @Query( " SELECT " +
            "   feedbackTemplate " +
            " FROM " +
            "   Terminal AS terminal " +
            "   LEFT JOIN" +
            "       FeedbackTemplate AS feedbackTemplate " +
            "           ON terminal.defaultTemplateId = feedbackTemplate.id " +
            " WHERE " +
            "   terminal.uuid = :uuid ")
    Optional<FeedbackTemplate> getFeedbackTemplateByTerminalUUID(UUID uuid);
}
