package ru.cubos.dataset.analysis;

import ru.cubos.dataset.entity.Category;
import ru.cubos.dataset.entity.DataSet;
import ru.cubos.dataset.entity.MapCategoryDto;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static ru.cubos.dataset.utils.NullUtil.zeroIfNull;

public class Analise {

    /**
     * Метод разбивает комментарий на предложения, а предложения на слова.
     * @param comment Текст комментария
     * @return Список предложение со списком слов
     */
    public static List<List<String>> parseComment(String comment) {
        if(excludeWords == null) {
            initExcludeWords();
        }
        return Arrays.stream(Arrays.stream(comment.split("\\."))
                .toArray()).map(e -> parseSentence(e.toString())).toList();
    }

    /**
     * Метод расбивает предложение на слова
     * @param sentence предложение
     * @return массив слов
     */
    public static List<String> parseSentence(String sentence) {
        return Arrays.stream(Arrays.stream(sentence.split("[^a-zA-Zа-яА-Я]")).toArray())
                .filter(e -> e.toString().length() > 0).map(Object::toString)
                .map(String::toLowerCase).filter(word -> !excludeWords.contains(word))
                .map(Analise::prepareWord).toList();
    }


    /**
     * Регулярное выражение для вырезки окончаний и/или приставок
     */
    //private static String regexp = "^при|аю$|у$";
    private static String regexp = "ая$|ее$|ют$|ю$|ет$|ут$|ет$|ят$|аю$|аем$|ешь$|ашь$|ает$|ал$|али$|ало$|ться$|тся$|ся$|а$|ия$|ы$|ие$|у$|ой$|ом$|ым$|ое$|о$|ы$|ия$|ие$|ию$|ии$|и$|е$|я$|ь$";

    /**
     * Получаем фразы из предложения
     * @param words набор слов
     * @return набор фраз
     */
    private static List<String> getPhrases(List<String> words) {
        List<String> phrases = new ArrayList<>();
        for (int i=1; i<words.size(); i++) {
            String word = words.get(i);
            String lastWord = words.get(i-1);

            phrases.add(word + " " + lastWord);
        }

        return phrases;
    }

    /**
     * Метод подготовки слова - убирает приставки и окончания
     * @param word слово
     * @return подготовленное слово
     */
    public static String prepareWord(String word) {
        if (word.length()>2) {
            word = word.replaceAll(regexp, "");
        }
        return word;
    }

    HashMap<String, HashMap<Integer, Integer>> categoryWord = new HashMap<>();
    //    HashMap<String, HashMap<Integer, Integer>> scoreWord = new HashMap<>();
    HashMap<String, HashMap<Integer, Integer>> categoryPhrase = new HashMap<>();
//    HashMap<String, HashMap<Integer, Integer>> scorePhrase = new HashMap<>();

    HashMap<Integer, Integer> totalDataSetCategoriesCount = new HashMap<>();

    private void setToValueMap(String key, HashMap<String,
            HashMap<Integer, Integer>> map, int value) {
        HashMap<Integer, Integer> valueSumMap = map.get(key);
        if (valueSumMap == null) {
            valueSumMap = new HashMap<>();
            map.put(key, valueSumMap);
        }
        valueSumMap.put(value, valueSumMap.get(value) == null ? 1 : (valueSumMap.get(value) + 1));
    }

    public void train(List<DataSet> learnDataSet) {
        learnDataSet.forEach( dataSet -> {
            totalDataSetCategoriesCount.put(dataSet.getCategory(),
                    totalDataSetCategoriesCount.get(dataSet.getCategory()) == null ? 1 :
                            totalDataSetCategoriesCount.get(dataSet.getCategory()) + 1);

            parseComment(dataSet.getComment()).forEach(sentence -> {
                sentence.stream().filter(word -> word.length() > 3)
                        .forEach(word -> {
                            setToValueMap(word, categoryWord, dataSet.getCategory());
//                    setToValueMap(word, scoreWord, dataSet.getFeedbackValue());
                        });

                getPhrases(sentence).forEach(phrase -> {
                    setToValueMap(phrase, categoryPhrase, dataSet.getCategory());
//                    setToValueMap(phrase, scorePhrase, dataSet.getFeedbackValue());
                });
            });
        });
    }

    public void sumMaps( HashMap<Integer, Integer> mapTo,
                         HashMap<Integer, Integer> mapFrom) {
        sumMaps( mapTo, mapFrom, 1.0);
    }

    public void sumMaps( HashMap<Integer, Integer> mapTo,
                         HashMap<Integer, Integer> mapFrom, Double koeff) {
        if (mapFrom != null) {
            mapFrom.keySet().forEach(key ->
                    mapTo.put(key, zeroIfNull(mapTo.get(key)) + (int)((double)mapFrom.get(key)/koeff * 1000) ));
        }
    }

    public void printMap(HashMap<Integer, Integer> map) {
        map.keySet().forEach(key -> System.out.println("    " + key + " -> " + map.get(key)));
    }

    public MapCategoryDto mapCategory(HashMap<Integer, Integer> map) {
        HashMap<Integer, Double> categoryMap = new HashMap<>();
        AtomicReference<Double> totalFound = new AtomicReference<>(0.0);

        AtomicReference<Integer> maxCategory = new AtomicReference<>(0);
        AtomicReference<Double> maxCategoryValue = new AtomicReference<>(0.0);

        map.keySet().forEach(key -> {
            Double categoryValue =
                    (double) map.get(key) / (double) totalDataSetCategoriesCount.get(key);

            if (maxCategoryValue.get() < categoryValue) {
                maxCategoryValue.set(categoryValue);
                maxCategory.set(key);
            }

//            System.out.println("    " + getCategoryName(key) + " -> " + categoryValue);

            totalFound.updateAndGet(v -> v + (double) map.get(key));
            categoryMap.put(key, categoryValue);
        });

        return new MapCategoryDto()
                .setCategory(Category.getCategoryByNum(maxCategory.get()))
                .setMap(categoryMap)
                .setSum(totalFound.get());
    }

    public Set<Category> feed(String comment){
        return parseComment(comment).stream().filter(sentence -> sentence.size() > 0).map(sentence -> {
//            System.out.println("\n Предложение: " + sentence);
            HashMap<Integer, Integer> resCategoryWord = new HashMap<>();
//            HashMap<Integer, Integer> resScoreWord = new HashMap<>();

            HashMap<Integer, Integer> resCategoryPhrase = new HashMap<>();
//            HashMap<Integer, Integer> resScorePhrase = new HashMap<>();

            HashMap<Integer, Integer> resCategoryFull = new HashMap<>();
//            HashMap<Integer, Integer> resScoreFull = new HashMap<>();

            sentence.forEach(word -> {
                HashMap<Integer, Integer> categoryMap = categoryWord.get(word);
//                HashMap<Integer, Integer> scoreMap = scoreWord.get(word);

                sumMaps(resCategoryWord, categoryMap);
//                sumMaps(resScoreWord, scoreMap);
            });

            getPhrases(sentence).forEach(word -> {
                HashMap<Integer, Integer> categoryMap = categoryPhrase.get(word);
//                HashMap<Integer, Integer> scoreMap = scorePhrase.get(word);

                sumMaps(resCategoryPhrase, categoryMap);
//                sumMaps(resScorePhrase, scoreMap);
            });

//            System.out.println("\nКатегория по словам");
            MapCategoryDto mapResByWord = mapCategory(resCategoryWord);
            HashMap<Integer, Double> categoryMapByWord = mapResByWord.getMap();
//            System.out.println("Оценка по словам");
//            printMap(resScoreWord);

//            System.out.println("\nКатегория по фразам");
            MapCategoryDto mapResByPhrase = mapCategory(resCategoryPhrase);
            HashMap<Integer, Double> categoryMapByPhrase = mapResByPhrase.getMap();
//            System.out.println("Оценка по фразам");
//            printMap(resScorePhrase);

            sumMaps(resCategoryFull, resCategoryWord, mapResByWord.getSum());
            sumMaps(resCategoryFull, resCategoryPhrase, mapResByPhrase.getSum());
//            System.out.println("Категория:");
            MapCategoryDto mapResTotal = mapCategory(resCategoryFull);
//            System.out.println("Оценка:");
//            printMap(resScoreFull);
            return mapResTotal.getCategory();
        }).collect(Collectors.toSet());
    }

    /**
     * Слова исключения, которые не точно не определяют категорию предложения
     */
    static public Set<String> excludeWords = null;

    /**
     * Формирование списка слов исключений
     */
    static public void initExcludeWords() {
        excludeWords = new HashSet<>();
        excludeWords.add(prepareWord("пришла"));
        excludeWords.add(prepareWord("пришло"));
        excludeWords.add(prepareWord("пришли"));
        excludeWords.add(prepareWord("доволен"));
        excludeWords.add(prepareWord("довольна"));
        excludeWords.add(prepareWord("довольны"));
        excludeWords.add(prepareWord("очень"));
        excludeWords.add(prepareWord("работает"));
        excludeWords.add(prepareWord("зачем"));
        excludeWords.add(prepareWord("такая"));
        excludeWords.add(prepareWord("расстроен"));
        excludeWords.add(prepareWord("расстроена"));
        excludeWords.add(prepareWord("оказалось"));
        excludeWords.add(prepareWord("ужас"));
        excludeWords.add(prepareWord("обидно"));
        excludeWords.add(prepareWord("товар"));
        excludeWords.add(prepareWord("отлично"));
//        excludeWords.add(prepareWord("большая"));
//        excludeWords.add(prepareWord("доставку"));
//        excludeWords.add(prepareWord("заказе"));
    }
}
