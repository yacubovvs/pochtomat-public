package ru.cubos.dataset.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DataSet {
    private String comment;
    private String timestamp;
    private int feedbackValue;
    private double price;
    private int category;

    public DataSet(String[] rawDataSet) {
        if(rawDataSet.length < 2) {
            System.out.println(rawDataSet[0] + " size is less 2 ");
        }
        if(rawDataSet.length > 2 && rawDataSet[2].trim().length() > 0) {
            System.out.println(rawDataSet[0] + " size is more 2 ");
        }
        comment = rawDataSet[0];
        //timestamp = rawDataSet[1];
        //feedbackValue = Integer.parseInt(rawDataSet[2]);
        //price = Double.parseDouble(rawDataSet[3]);
        try {
            category = Integer.parseInt(rawDataSet[1].trim());
        } catch (Exception e) {
            System.out.println("Cant parse comment " + comment);
        }
    }
}
