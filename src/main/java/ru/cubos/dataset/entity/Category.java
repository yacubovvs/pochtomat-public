package ru.cubos.dataset.entity;


public enum Category {
    GOODS_QUALITY(1, "Качество товара"),
    GOODS_COMPOSITION(2, "Состав товара"),
    DELIVERY_TIME(3, "Срок доставки"),
    PACKAGING(4, "Качество упаковки"),
    POSTOMAT_WORK(5, "Работа постамата"),
    NO_CATEGORY(0, "Без категории");

    private int num;
    private String name;

    Category(int num, String name) {
        this.num = num;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Category getCategoryByNum(int num) {
        switch (num) {
            case 1: return GOODS_QUALITY;
            case 2: return GOODS_COMPOSITION;
            case 3: return DELIVERY_TIME;
            case 4: return PACKAGING;
            case 5: return POSTOMAT_WORK;
            case 0:
            default: return NO_CATEGORY;
        }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
