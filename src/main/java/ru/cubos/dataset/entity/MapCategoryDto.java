package ru.cubos.dataset.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;

@Data
@Accessors(chain = true)
public class MapCategoryDto {
    private HashMap<Integer, Double> map;
    private Double sum;
    private Category category;
}
