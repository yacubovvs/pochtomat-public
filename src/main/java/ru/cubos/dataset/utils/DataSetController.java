package ru.cubos.dataset.utils;

import ru.cubos.dataset.entity.DataSet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataSetController {
    public static List<DataSet> getDataSet() {
        return getRawDataSet(null).stream().map(DataSet::new).toList();
    }

    public static List<DataSet> getDataSet(String path) {
        return getRawDataSet(path).stream().map(DataSet::new).toList();
    }

    private static List<String[]> getRawDataSet(String path) {
        if (path == null) {
            path = "dataset/commentsDataset.csv";
        }
        List<String[]> rawDataset = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while (true) {
                line = br.readLine();
                if (line!= null) {
                    rawDataset.add(line.split(";"));
                } else {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return rawDataset;
    }
}
