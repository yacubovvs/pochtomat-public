package ru.cubos.dataset.utils;

public class NullUtil {
    static public Integer zeroIfNull(Integer value) {
        return value == null ? 0 : value;
    }
}
