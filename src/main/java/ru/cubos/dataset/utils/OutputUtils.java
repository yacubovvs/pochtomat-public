package ru.cubos.dataset.utils;

public class OutputUtils {
    static public String getCategoryName(Integer i) {
        if (i == 0 || i == null) {
            return "Нет категории";
        }

        switch (i) {
            case 1: return "Качество товара";
            case 2: return "Состав доставки";
            case 3: return "Срок доставки";
            case 4: return "Качество упаковки";
            case 5: return "Работа постамата";
            default: return "Нет категории";
        }
    }
}
