FROM openjdk:17-oracle

WORKDIR /opt/app

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
COPY dataset/commentsDataset.csv /opt/commentsDataset.csv
COPY src ./src
RUN chmod +x ./mvnw
CMD ["./mvnw", "spring-boot:run"]