#clear
echo "START BUILDING"
cd src && mvn clean compile -DSKIP_TESTS="true" -DskipTests="true" spring-boot:build-image
cp target/*.jar ../app.jar
cd ..
echo "START TOMCAT 2"
java -jar app.jar
